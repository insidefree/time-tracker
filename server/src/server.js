const express = require('express')
const app = express()
const server = require('http').createServer(app);

// const io = require('socket.io')(server, {
//     path: '/test',
//     serveClient: false,
//     // below are engine.IO options
//     pingInterval: 10000,
//     pingTimeout: 5000,
//     cookie: false
// });

server.listen(3000);
app.get('/app', (req, res) => { res.sendFile(__dirname + '/index.html') })
app.get('/test', (req, res) => { res.send({status: 200}) })